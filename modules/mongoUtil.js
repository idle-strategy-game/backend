const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017";

var dbs = {};

const mongoOpts = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

module.exports = {
  connect: (callback) => {
    MongoClient.connect(url, mongoOpts, (err, client) => {
      dbs['users'] = client.db('users');
      dbs['world'] = client.db('world');
      return callback(err);
    });
  },

  getDb: (db) => {
    return dbs[db];
  },

  getDbs: () => {
    return dbs;
  }
};