var worldCreator = {
  voronoi: null,
  diagram: null,
  dim: null,
  margin: 0.1,
  bbox: null,
  sites: [],
  timeoutDelay: 100,
  np: 100,

  init: function () {
    this.randomSites(this.np, true);
  },

  clearSites: function () {
    this.compute([]);
  },

  randomSites: function (n, clear) {
    var sites = [];
    if (!clear) {
      sites = this.sites.slice(0);
    }
    // create vertices
    var xmargin = this.dim.width * this.margin,
      ymargin = this.dim.height * this.margin,
      xo = xmargin,
      dx = this.dim.width - xmargin * 2,
      yo = ymargin,
      dy = this.dim.height - ymargin * 2;
    for (var i = 0; i < n; i++) {
      sites.push({ x: Math.round((xo + Math.random() * dx) * 10) / 10, y: Math.round((yo + Math.random() * dy) * 10) / 10 });
    }
    this.compute(sites);
    // relax sites
    // if (this.timeout) {
    //   clearTimeout(this.timeout)
    //   this.timeout = null;
    // }
    // var me = this;
    // this.timeout = setTimeout(function () {
    //   me.relaxSites();
    // }, this.timeoutDelay);
  },

  relaxSites: function () {
    if (!this.diagram) { return; }
    var cells = this.diagram.cells,
      iCell = cells.length,
      cell,
      site, sites = [],
      again = false,
      rn, dist;
    var p = 1 / iCell * 0.1;
    while (iCell--) {
      cell = cells[iCell];
      rn = Math.random();
      // probability of apoptosis
      if (rn < p) {
        continue;
      }
      site = this.cellCentroid(cell);
      dist = this.distance(site, cell.site);
      // again = again || dist > 1;
      // don't relax too fast
      if (dist > 2) {
        site.x = (site.x + cell.site.x) / 2;
        site.y = (site.y + cell.site.y) / 2;
      }
      // probability of mytosis
      if (rn > (1 - p)) {
        dist /= 2;
        sites.push({
          x: site.x + (site.x - cell.site.x) / dist,
          y: site.y + (site.y - cell.site.y) / dist,
        });
      }
      sites.push(site);
    }
    this.compute(sites);
    // if (again) {
    //   var me = this;
    //   this.timeout = setTimeout(function () {
    //     me.relaxSites();
    //   }, this.timeoutDelay);
    // }
  },

  distance: function (a, b) {
    var dx = a.x - b.x,
      dy = a.y - b.y;
    return Math.sqrt(dx * dx + dy * dy);
  },

  cellArea: function (cell) {
    var area = 0,
      halfedges = cell.halfedges,
      iHalfedge = halfedges.length,
      halfedge,
      p1, p2;
    while (iHalfedge--) {
      halfedge = halfedges[iHalfedge];
      p1 = halfedge.getStartpoint();
      p2 = halfedge.getEndpoint();
      area += p1.x * p2.y;
      area -= p1.y * p2.x;
    }
    area /= 2;
    return area;
  },

  cellCentroid: function (cell) {
    var x = 0, y = 0,
      halfedges = cell.halfedges,
      iHalfedge = halfedges.length,
      halfedge,
      v, p1, p2;
    while (iHalfedge--) {
      halfedge = halfedges[iHalfedge];
      p1 = halfedge.getStartpoint();
      p2 = halfedge.getEndpoint();
      v = p1.x * p2.y - p2.x * p1.y;
      x += (p1.x + p2.x) * v;
      y += (p1.y + p2.y) * v;
    }
    v = this.cellArea(cell) * 6;
    return { x: x / v, y: y / v };
  },

  compute: function (sites) {
    this.sites = sites;
    this.voronoi.recycle(this.diagram);
    this.diagram = this.voronoi.compute(sites, this.bbox);
  },

  affectTypeCells: function () {
    // Water on border
    this.diagram.cells.forEach((cell, i) => {
      cell.voronoiId = cell.site.voronoiId;
      cell.neighboors = [];

      // Affect water type on all bounding cells
      cell.halfedges.forEach((halfedge, j) => {
        let lSite = halfedge.edge.lSite;
        let rSite = halfedge.edge.rSite;
        if (lSite !== null && !cell.neighboors.includes(lSite.voronoiId) && lSite.voronoiId !== cell.voronoiId) {
          cell.neighboors.push(lSite.voronoiId)
        }
        if (rSite !== null && !cell.neighboors.includes(rSite.voronoiId) && rSite.voronoiId !== cell.voronoiId) {
          cell.neighboors.push(rSite.voronoiId)
        }

        if (halfedge.edge.lSite == null || halfedge.edge.rSite == null) {
          cell.type = 'water';
        }
      });
    });
  },

  createTypeCells: function (type, probability) {
    this.diagram.cells.forEach((cell, i) => {
      if (cell.type == null && Math.random() > 1 - probability) {
        cell.type = type;
      }
    });
  },

  propagateWater: function (coeff) {
    // Get array of tuples : { cell, waterNeighboors }
    let allCells = this.diagram.cells;
    let tuples = allCells.map(cell => {
      return {
        cell: cell,
        waterNeighboors: allCells.filter(other => other.type === 'water' && other.neighboors.includes(cell.voronoiId)).length
      }
    });


    tuples.forEach((tuple, i) => {
      if (tuple.cell.type == null && Math.random() > (tuple.waterNeighboors != 0 ? 1 / (tuple.waterNeighboors * coeff) : Infinity)) {
        tuple.cell.type = 'water';
      }
    });
  },

  getClockwisePoints: function () {
    this.diagram.cells.forEach((cell, i) => {
      let points = [];

      // Get unique points
      cell.halfedges.forEach((halfedge, j) => {
        if (!points.includes(points.find(e => e.x === halfedge.edge.va.x && e.y === halfedge.edge.va.y))) {
          points.push(halfedge.edge.va);
        }
        if (!points.includes(points.find(e => e.x === halfedge.edge.vb.x && e.y === halfedge.edge.vb.y))) {
          points.push(halfedge.edge.vb);
        }

        // Add an angle property to each point using tan(angle) = y/x
        const angles = points.map(({ x, y }) => {
          return { x, y, angle: Math.atan2(y - cell.site.y, x - cell.site.x) * 180 / Math.PI };
        });

        // Sort points by angle
        cell.pointsSorted = angles.sort((a, b) => a.angle - b.angle);
      })
    });
  },

  getNoisedDiagram: function (sub, minLength, amp) {
    this.newDiagram = this.diagram;

    let isConvex = (quad) => {
      if (quad.length < 4)
        return true;

      let sign = false;
      let n = quad.length;

      for (let i = 0; i < n; i++) {
        let dx1 = quad[(i + 2) % n].x - quad[(i + 1) % n].x;
        let dy1 = quad[(i + 2) % n].y - quad[(i + 1) % n].y;
        let dx2 = quad[i].x - quad[(i + 1) % n].x;
        let dy2 = quad[i].y - quad[(i + 1) % n].y;
        let zcrossproduct = dx1 * dy2 - dy1 * dx2;

        if (i == 0)
          sign = zcrossproduct > 0;
        else if (sign != (zcrossproduct > 0)) {
          console.log('Not convex ! Skipping subdivision of this edge.')
          return false;
        }
      }
      return true;
    }

    let getDivisionPoint = (p1, p2, u) => {
      // Pick random point between both sites with : (1-u)*p1 + u*p2
      return { x: (1 - u) * p1.x + u * p2.x, y: (1 - u) * p1.y + u * p2.y }
    };

    // Recursively subdivide an edge with random noise
    let noisyEdges = (edge, depth) => {
      // Compute edge length
      let a = edge.va.x - edge.vb.x, b = edge.va.y - edge.vb.y;
      let length = Math.sqrt(a * a + b * b);

      // Recursion stop condition
      if (/* depth == 0 
      || */ edge.lSite == null || edge.rSite == null // Bounding cell
        || length < minLength // Edge too small
        || !isConvex([edge.va, edge.lSite, edge.vb, edge.rSite])) { // Quad is not convex
        return edge;
      } else {
        let random = 0.5 * Math.random() + 0.25; // Used to get random midpoint
        let edgeleft = {
          lSite: getDivisionPoint(edge.lSite, edge.va, 0.5),
          rSite: getDivisionPoint(edge.rSite, edge.va, 0.5),
          va: edge.va,
          vb: getDivisionPoint(edge.lSite, edge.rSite, random)
        }
        edgeleft.lSite.voronoiId = edge.lSite.voronoiId;
        edgeleft.rSite.voronoiId = edge.rSite.voronoiId;

        let edgeRight = {
          lSite: getDivisionPoint(edge.lSite, edge.vb, 0.5),
          rSite: getDivisionPoint(edge.rSite, edge.vb, 0.5),
          va: getDivisionPoint(edge.lSite, edge.rSite, random),
          vb: edge.vb
        }
        edgeRight.lSite.voronoiId = edge.lSite.voronoiId;
        edgeRight.rSite.voronoiId = edge.rSite.voronoiId;

        return [].concat(noisyEdges(edgeleft, depth - 1)).concat(noisyEdges(edgeRight, depth - 1));
      }
    }

    // Replace edges by their subdivision
    let newEdges = []
    this.diagram.edges.forEach((edge, i) => {
      newEdges = newEdges.concat(noisyEdges(edge, sub));
    });
    this.newDiagram.edges = newEdges;

    // Replace halfedges in all cells consequently
    this.diagram.cells.forEach((cell, i) => {
      let newHalfedges = []
      cell.halfedges.forEach((halfedge, j) => {
        if (halfedge.edge.lSite == null || halfedge.edge.rSite == null) {
          // Don't noise border edges
          newHalfedges = newHalfedges.concat(halfedge)
        } else {
          // Find corresponding edges (result of subdivision)
          let newEdges = this.newDiagram.edges.filter((e) => e.lSite != null && e.rSite != null && e.lSite.voronoiId === halfedge.edge.lSite.voronoiId && e.rSite.voronoiId === halfedge.edge.rSite.voronoiId)
          newHalfedges = newHalfedges.concat(newEdges.map((e) => { return { edge: e } }));
        }
      });
      // if (i == 20) {
      //   console.log('old');
      //   console.log(JSON.stringify(cell.halfedges));
      //   console.log('new');
      //   console.log(JSON.stringify(newHalfedges));
      // }
      this.newDiagram.cells[i].halfedges = newHalfedges;
    })

    // Finally, replace diagram
    this.diagram = this.newDiagram;
  }
};

module.exports = { worldCreator: worldCreator };