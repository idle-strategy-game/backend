const express = require('express');
const path = require('path');
const fs = require('fs');
const sass = require('node-sass-middleware');
const mongo = require('./modules/mongoUtil'); //require('mongodb').MongoClient;
const bcrypt = require('bcrypt');

const app = express();
const cellWorld = require('./routes/cellWorld');
const hexWorld = require('./routes/hexWorld');
const port = 8081;

const UPDATE_DELAY = 60000; // ms

// First
app.use(sass({
  /* Options */
  src: path.join(__dirname + '/sass'),
  dest: path.join(__dirname, '/public/css'),
  debug: true,
  outputStyle: 'expanded',
  prefix: '/css',
}));

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());

app.get('/', (req, res) => {
  res.send('Backend home page');
});

app.use('/hexWorld', hexWorld);
app.use('/cellWorld', cellWorld);

app.post('/register', async (req, res) => {
  try {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(req.body.password, salt);

    const user = JSON.parse(fs.readFileSync('./public/assets/json/user.json'));
    user.username = req.body.username;
    user.password = hash;
    user.email = req.body.email;

    const db = mongoUtil.getDb('users');
    const result = await db.collection('users').findOne({ username: req.body.username });
    if (result) {
      res.status(400).send('User already exists');
    } else {
      await db.collection('users').insertOne(user);
      console.log(`New user registered: ${user.username}`);
      res.status(200).send('User registered');
    }
  } catch (err) {
    console.log(err);
    res.status(500).send('Internal error whil registering a new user.');
  }
});

app.post('/authenticate', async (req, res) => {
  try {
    const db = mongoUtil.getDb('users');
    const user = await db.collection('users').findOne({ username: req.body.username });
    if (!user) {
      res.status(400).send('User doesn\'t exist.');
    } else {
      check = await bcrypt.compare(req.body.password, user.password);
      if (check) {
        res.status(200).send('Logged in.');
      } else {
        res.status(400).send('Wrong password.');
      }
    }
  } catch (err) {
    res.status(500).send('Internal error while authenticating user.');
  }
});

app.get('/resources', async (req, res) => {
  try {
    const db = mongoUtil.getDb('users');
    user = await db.collection('users').findOne({ username: req.query.username })
    res.status(200).json({
      "wood": user.resources.wood,
      "stone": user.resources.stone,
      "gold": user.resources.gold,
      "food": user.resources.food,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send('Internal error while fetching resources.');
  }
});

mongo.connect((err, client) => {
  if (err) {
    console.log(err)
  } else {
    app.listen(port, () => {
      console.log(`Express running on port ${port}!`)

      /* Periodically update resources */
      setInterval(() => {
        mongo.getDb('users').collection('users').find({}).toArray((err, users) => {
          if (err) {
            console.log('Error while updating resources.');
          } else {
            users.forEach((user, i) => {
              deltas = user.deltas;
              mongo.getDb('users').collection('users').updateOne({ _id: user._id }, {
                "$set": {
                  "resources": {
                    "wood": user.resources.wood + deltas.dWood,
                    "stone": user.resources.stone + deltas.dStone,
                    "gold": user.resources.gold + deltas.dGold,
                    "food": user.resources.food + deltas.dFood
                  }
                }
              });
            });
          }
        });
        console.log("Resources updated.");
      }, UPDATE_DELAY);
    });
  }
})