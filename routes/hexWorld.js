var express = require('express');
var router = express.Router();

router.get('/', (req, res) => {
  res.send('Hex world home page');
});

router.get('/init', (req, res) => {
  res.send('Init hex world.');
});

module.exports = router;