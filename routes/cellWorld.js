const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const { createCanvas } = require('canvas');
const Voronoi = require('../modules/rhill-voronoi-core');
const worldCreator = require('../modules/worldCreator').worldCreator;
const mongoUtil = require('../modules/mongoUtil');

router.get('/init', async (req, res) => {
  // Read world config file
  json = fs.readFileSync('./public/assets/json/world.json');
  const conf = JSON.parse(json);

  worldCreator.voronoi = new Voronoi();
  worldCreator.bbox = {
    xl: conf.xl, // req.query.xl,
    xr: conf.xr,
    yt: conf.yt, // req.query.yt,
    yb: conf.yb
  }
  worldCreator.dim = { width: parseInt(conf.xr), height: parseInt(conf.yb) };

  worldCreator.randomSites(conf.np);

  for (i = 0; i < conf.lloyd; i++) {
    worldCreator.relaxSites();
  }

  worldCreator.affectTypeCells();
  worldCreator.createTypeCells('water', conf.pbWater); // Lakes
  worldCreator.propagateWater(conf.waterPropagation);
  worldCreator.createTypeCells('forest', conf.pbForest);
  worldCreator.createTypeCells('stonePit', conf.pbStonePit);
  worldCreator.createTypeCells('goldMine', conf.pbGoldMine);
  if (conf.noise) worldCreator.getNoisedDiagram(conf.noiseDepth, conf.minLength, null);
  worldCreator.getClockwisePoints();

  try {
    const db = mongoUtil.getDb('world');
    await db.collection('sites').insertMany(worldCreator.sites);
    await db.collection('vertices').insertMany(worldCreator.diagram.vertices);
    await db.collection('edges').insertMany(worldCreator.diagram.edges);
    await db.collection('cells').insertMany(worldCreator.diagram.cells);
    await db.collection('meta').insertOne(worldCreator.dim);
    res.status(200).send('World generated.');
  } catch (err) {
    res.status(500).send('Internal error while generating the world.');
  }
});

router.get('/clean', async (req, res) => {
  try {
    const db = mongoUtil.getDb('world');
    await db.collection('cells').deleteMany({});
    await db.collection('edges').deleteMany({});
    await db.collection('vertices').deleteMany({});
    await db.collection('sites').deleteMany({});
    res.status(200).send('World cleaned');
  } catch (err) {
    console.log(err);
    res.status(500).send('Internal error while cleaning the world');
  }
});

router.get('/display', (req, res) => {
  res.sendFile(path.join(__dirname, '../html/world.html'));
});

router.get('/data', async (req, res) => {
  try {
    const db = mongoUtil.getDb('world');
    const cells = await db.collection('cells').find({}).toArray();
    res.status(200).json({ cells: cells });
  } catch (err) {
    console.log(err);
    res.status(500).send('Internal error while fetching world data.');
  }
});

router.get('/canvas', async (req, res) => {
  // Read world config file
  console.log(__dirname);
  json = fs.readFileSync(path.join(__dirname, '../public/assets/json/world.json'));
  const conf = JSON.parse(json);

  try {
    const db = mongoUtil.getDb('world');
    const meta = await db.collection('meta').findOne({});
    const canvas = createCanvas(parseInt(meta.width), parseInt(meta.height));
    const ctx = canvas.getContext('2d');
    ctx.fillStyle = "#3978a8";
    ctx.fillRect(0, 0, parseInt(meta.width), parseInt(meta.height));

    cells = await db.collection('cells').find({}).toArray();

    cells.forEach((cell, i) => {
      // Walk path clockwise
      let points = [];
      ctx.beginPath();
      for (let i = 0; i < cell.pointsSorted.length; i++) {
        if (i == 0) {
          ctx.beginPath();
          ctx.moveTo(cell.pointsSorted[i].x, cell.pointsSorted[i].y);
        } else {
          ctx.lineTo(cell.pointsSorted[i].x, cell.pointsSorted[i].y);
        }
      }
      ctx.closePath();

      switch (cell.type) {
        case 'water': ctx.fillStyle = conf.colorWater; break;
        case 'grass': ctx.fillStyle = conf.colorGrass; break;
        case 'forest': ctx.fillStyle = conf.colorForest; break;
        case 'stonePit': ctx.fillStyle = conf.colorRocks; break;
        case 'goldMine': ctx.fillStyle = conf.colorGold; break;
        default: ctx.fillStyle = conf.colorGrass; break;
      }
      ctx.fill();

      // Draw edges & centroids
      if (cell.type !== 'water') {
        ctx.strokeStyle = '#ffffff';
        ctx.fillStyle = '#ffffff';
        ctx.lineWidth = 3;
        ctx.stroke();
        ctx.fillRect(cell.site.x - 3, cell.site.y - 3, 6, 6);
      }
    });
    res.status(200).send(canvas.toDataURL());
  } catch (err) {
    console.log(err);
    res.status(500).send('Error while drawing world canvas.');
  }
});

module.exports = router;